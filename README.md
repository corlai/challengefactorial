# ChallengeFactorial

Solution of the challenge proposed by factorial

## Requirements 
- Docker
- Docker-compose
- .env files in the root of the client and server (you can copy the .env.example file)

## Execute
- From root: ``` docker-compose up ```

## Down
- From root: ``` docker-compose down -v ```
 