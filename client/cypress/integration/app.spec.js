import { aliasQuery, aliasMutation } from '../utils/graphql-test-utils'

describe('Home Metrics', () => { 
    it('successfully loads', () => {
        cy.visit('/') // change URL to match your dev URL
    })
    it('should navigate to the analytics component', () => {
      // Start from the index page
      cy.visit('/')
  
      // The new page should contain an h1 with "About page"
      cy.get('h1').contains('Analytics')
    })
  })
