import { aliasQuery, aliasMutation } from '../utils/graphql-test-utils'
import { GET_METRICS_FILTER } from '../../src/graphql/query'

describe('Query All Metrics', () => { 
    it('successfully query', () => {
        cy.request({
            method: 'POST',
            url: 'http://localhost:4000/graphql',
            body: {
              query: `
                query GetMetricsFilter($filter: TypeFilter) {
                    getMetricsFilter(filter: $filter) {
                        timestamp
                        avgValue
                        count
                    }}
              `,
              variables: {
                filter: "HR"
                }
            },
          }).should(
            (response) => {
              expect(response.status).to.eq(200)
              expect(response.body)
                .to.have.property('data')
              expect(response.body.data)
               .to.have.property('getMetricsFilter')
              expect(response.body.data.getMetricsFilter)
               .to.have.property('length')
            })
    })
  })
