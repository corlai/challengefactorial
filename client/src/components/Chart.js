import React, { useState } from "react";
import { useQuery } from '@apollo/client';

import { GET_METRICS_FILTER } from '../graphql/query'
import ChartFilter from "./ChartFilter";
import Card from "./Card";
import {
    Area,
    XAxis,
    YAxis,
    ResponsiveContainer,
    AreaChart,
    Tooltip
} from "recharts";

const CustomTooltip = ({ active, payload, label, filter }) => {
    if (active && payload && payload.length) {
        return (
            <div className="bg-zinc-100 hover:bg-blue-800 focus:ring-4 focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">
                <p className="label">Average: <span className="font-bold">{`${payload[0].value}`}</span></p>
                <p className="desc">{getFormat(filter, payload[0].payload.date)}</p>
            </div>
        );
    }

    return null;
};

const getFormat = (filter, date) => {
    let d = new Date(date)
    if (filter === 'MIN') {
        return `${d.getFullYear()}-${d.getMonth()+1}-${d.getDate()} (${d.getHours()} hour : ${d.getMinutes()} minute)`;
    }
    if (filter === 'HR') {
        return `${d.getFullYear()}-${d.getMonth()+1}-${d.getDate()} (${d.getHours()} hour)`;
    }
    if (filter === 'DAY') {
        return `${d.getFullYear()}-${d.getMonth()+1}-${d.getDate()}`;
    }
    return '';
};

export const chartConfig = {
    "MIN": { resolution: "1", minutes: 1, hours: 0, days: 0, weeks: 0, months: 0, years: 0 },
    "HR": { resolution: "1", minutes: 0, hours: 1, days: 0, weeks: 0, months: 0, years: 0 },
    "DAY": { resolution: "1", minutes: 0, hours: 0, days: 1, weeks: 0, months: 0, years: 0 },
  };

const Chart = ({ children }) => {
    const [filter, setFilter] = useState("MIN");
    const { loading, error, data } = useQuery(GET_METRICS_FILTER, { variables: { filter: filter } });

    if (loading) return <p>Loading...</p>;
    if (error) return <p>Error :(</p>;

    if (data) {
        const formatData = (data) => {
            return data.map(item => {
                return {
                    value: item.avgValue,
                    date: item.timestamp,
                };
            });
        };

        return (
            <Card>
                <ul className="flex absolute top-2 right-2 z-40">
                    {Object.keys(chartConfig).map((item) => (
                        <li key={item}>
                            <ChartFilter
                                text={item}
                                active={filter === item}
                                onClick={() => {
                                    setFilter(item);
                                }}
                            />
                        </li>
                    ))}
                </ul>
                <ResponsiveContainer>
                    <AreaChart data={formatData(data.getMetricsFilter)}>
                        <defs>
                            <linearGradient id="chartColor" x1="0" y1="0" x2="0" y2="1">
                                <stop
                                    offset="5%"
                                    stopColor="rgb(255 53 94)"
                                    stopOpacity={0.8}
                                />
                                <stop
                                    offset="95%"
                                    stopColor="rgb(255 53 94)"
                                    stopOpacity={0}
                                />
                            </linearGradient>
                        </defs>
                        <Area
                            type="monotone"
                            dataKey="value"
                            stroke="#25253d"
                            fill="url(#chartColor)"
                            fillOpacity={1}
                            strokeWidth={0.5}
                        />
                        <XAxis dataKey="date" />
                        <YAxis domain={[0, "dataMax"]} />
                        <Tooltip content={<CustomTooltip filter={filter} />} />
                    </AreaChart>
                </ResponsiveContainer>
            </Card>
        );

    }
};

export default Chart;