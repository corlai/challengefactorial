import React from "react";

const ChartFilter = ({ text, active, onClick }) => {
	return (
		<button
			onClick={onClick}
			className={`w-12 m-2 h-8 border-1 rounded-md flex items-center justify-center cursor-pointer ${active
					? "bg-red-500 border-red-500 text-gray-100"
					: "border-red-300 text-red-300"
				} transition duration-200 hover:bg-red-500 hover:text-gray-100 hover:border-red-500 z-10`}
		>
			{text}
		</button>
	);
};

export default ChartFilter;