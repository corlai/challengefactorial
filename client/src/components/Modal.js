import { Fragment } from 'react'
import { Dialog, Transition } from '@headlessui/react'

import { useForm } from "react-hook-form";
import * as yup from 'yup'
import { yupResolver } from '@hookform/resolvers/yup'

import { useMutation } from '@apollo/client';
import { ADD_METRIC } from '../graphql/mutation'

export default function Modal({ open, setOpen }) {

    const metricSchema = yup.object().shape({
        name: yup.string().required("Name is required"),
        value: yup.number()
            .max(100, 'The number must be between 0 to 100')
            .min(0, 'The number must be between 0 to 100')
            .required('Value is required')
    })

    const [addMetric] = useMutation(ADD_METRIC);
    const { register, formState: { errors }, handleSubmit } = useForm({ resolver: yupResolver(metricSchema) });

    const handleAddMetric = async (data) => {
        try {
            await addMetric({
                variables: {
                    data: {
                        name: data.name,
                        timestamp: data.timestamp,
                        value: data.value
                    }
                }
            });
            setOpen(false)
        } catch (err) {
            console.log(err);
        }
    }

    return (
        <Transition.Root show={open} as={Fragment}>
            <Dialog as="div" className="fixed z-50 inset-0 overflow-y-auto" onClose={setOpen}>
                <div className="flex items-end justify-center min-h-screen pt-4 px-4 pb-20 text-center sm:block sm:p-0">
                    <Transition.Child
                        as={Fragment}
                        enter="ease-out duration-300"
                        enterFrom="opacity-0"
                        enterTo="opacity-100"
                        leave="ease-in duration-200"
                        leaveFrom="opacity-100"
                        leaveTo="opacity-0"
                    >
                        <Dialog.Overlay className="fixed inset-0 bg-gray-500 bg-opacity-75 transition-opacity" />
                    </Transition.Child>

                    {/* This element is to trick the browser into centering the modal contents. */}
                    <span className="hidden sm:inline-block sm:align-middle sm:h-screen" aria-hidden="true">
                        &#8203;
                    </span>
                    <Transition.Child
                        as={Fragment}
                        enter="ease-out duration-300"
                        enterFrom="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
                        enterTo="opacity-100 translate-y-0 sm:scale-100" s
                        leave="ease-in duration-200"
                        leaveFrom="opacity-100 translate-y-0 sm:scale-100"
                        leaveTo="opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95"
                    >
                        <form
                            className="max-w-xl m-auto py-10 mt-10 px-12"
                            onSubmit={handleSubmit(handleAddMetric)}
                        >
                            <div className="inline-block align-bottom bg-white rounded-lg text-left overflow-hidden shadow-xl transform transition-all sm:my-8 sm:align-middle sm:max-w-lg sm:w-full">

                                <div className="bg-white px-4 pt-5 pb-4 sm:p-6 sm:pb-4">
                                    <div className="sm:flex sm:items-start">
                                        <div className="mt-3 text-center sm:mt-0 sm:ml-4 sm:text-left">
                                            <Dialog.Title as="h3" className="text-lg leading-6 font-medium text-gray-900">
                                                New Metric
                                            </Dialog.Title>
                                            <div className="mt-2">
                                                <label className="text-gray-600 font-medium">Name</label>
                                                <input
                                                    className="border-solid border-gray-300 border py-2 px-4 w-full rounded text-gray-700"
                                                    name="name"
                                                    type="text"
                                                    placeholder="Write a name of metric"
                                                    autoFocus
                                                    {...register("name", {
                                                        required: "Please enter a name of metric",
                                                    })}
                                                />
                                                {errors && errors.name && (
                                                    <div className="mb-3 text-normal text-red-500">
                                                        {errors.name.message}
                                                    </div>
                                                )}

                                                <label className="text-gray-600 font-medium block mt-4">Value</label>
                                                <input
                                                    className="border-solid border-gray-300 border py-2 px-4 w-full rounded text-gray-700"
                                                    name="value"
                                                    type="number"
                                                    defaultValue={0}
                                                    placeholder="Write a value"
                                                    {...register("value", {
                                                        required: "Please enter a value",
                                                    })}
                                                />
                                                {errors.value && (
                                                    <div className="mb-3 text-normal text-red-500 ">
                                                        {errors.value.message}
                                                    </div>
                                                )}

                                                <label className="text-gray-600 font-medium block mt-4">Timestamp</label>
                                                <div className="relative">
                                                    <input
                                                        datepicker=""
                                                        type="datetime-local"
                                                        name="timestamp"
                                                        className="border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                                                        placeholder="Select date"
                                                        {...register("timestamp", {
                                                            required: "Please select a date",
                                                        })}
                                                    />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="bg-gray-50 px-4 py-3 sm:px-6 sm:flex sm:flex-row-reverse">
                                    <button
                                        type="submit"
                                        className="w-full inline-flex justify-center rounded-md border border-transparent shadow-sm px-4 py-2 bg-red-600 text-base font-medium text-white hover:bg-red-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-red-500 sm:ml-3 sm:w-auto sm:text-sm"
                                    >
                                        Create
                                    </button>
                                    <button
                                        type="button"
                                        className="mt-3 w-full inline-flex justify-center rounded-md border border-gray-300 shadow-sm px-4 py-2 bg-white text-base font-medium text-gray-700 hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500 sm:mt-0 sm:ml-3 sm:w-auto sm:text-sm"
                                        onClick={() => setOpen(false)}
                                    >
                                        Cancel
                                    </button>
                                </div>
                            </div>
                        </form>
                    </Transition.Child>
                </div>
            </Dialog>
        </Transition.Root >
    )
}
