import gql from 'graphql-tag';

export const ADD_METRIC = gql`
mutation AddMetric($data: MetricCreateInput) {
  addMetric(data: $data) {
    id
    name
    timestamp
  }
}
`;