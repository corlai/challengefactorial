import gql from 'graphql-tag';

export const GET_METRICS_FILTER = gql`
query GetMetricsFilter($filter: TypeFilter) {
  getMetricsFilter(filter: $filter) {
    timestamp
    avgValue
    count
  }
}
`;