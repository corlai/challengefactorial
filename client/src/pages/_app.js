import { ApolloProvider, ApolloClient, InMemoryCache } from '@apollo/client'
import '../styles/tailwind.css'

const baseUrl = process.env.NEXT_PUBLIC_SERVICE_URL || 'http://localhost:4000/'

export const client = new ApolloClient({
  uri: baseUrl,
  cache: new InMemoryCache()
})

function MyApp({ Component, pageProps }) {
  return (
    <ApolloProvider client={client}>
      <Component {...pageProps} />
    </ApolloProvider>
  )
}

export default MyApp
