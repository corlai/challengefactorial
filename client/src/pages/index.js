import React, { useState } from "react";
import Card from "../components/Card"
import Chart from "../components/Chart";
import Modal from "../components/Modal";

const dashboard = () => {
    const [open, setOpen] = useState(false)

    return (
        <main className="main-content flex-1 mt-12 md:mt-2 pb-24 md:pb-5">
            <Card >
                <div className="">
                    <div className="rounded-tl-3xl rounded-tr-3xl bg-gradient-to-r from-red-500 to-gray-400 p-4 text-2xl text-white">
                        <h1 className="font-bold pl-2">Analytics</h1>
                    </div>
                </div>
                <div className="px-4 sm:px-6 lg:px-8 py-8 w-full max-w-9xl border">

                    {/* <!-- Page header --> */}
                    <div className="sm:flex sm:justify-between sm:items-center mb-8">

                        {/* <!-- Left: Title --> */}
                        <div className="mb-4 sm:mb-0">
                            <h1 className="text-xl text-gray-700 font-bold">Metrics for Factorial</h1>
                        </div>

                        {/* <!-- Right: Actions --> */}
                        <div className="grid grid-flow-col sm:auto-cols-max justify-start sm:justify-end gap-2">
                            <button className="bg-red-500 hover:bg-red-700 text-white font-bold py-2 px-4 rounded"
                                onClick={() => setOpen(true)}
                            >
                                New metric
                            </button>

                        </div>

                    </div>

                    {/* <!-- Cards --> */}
                    <div className="grid grid-cols-12 gap-6">
                        <div className="flex flex-col col-span-full xl:col-span-8 bg-white shadow-lg rounded-sm border border-gray-200">
                            <div className="w-full p-3">
                                <div className="min-h-screen grid grid-cols-1">
                                    <div className="md:col-span-2 row-span-4 content-center">
                                        <Chart />
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>


                </div>
                <Modal
                    open={open}
                    setOpen={setOpen}
                />
            </Card>
        </main>
    );
};

export default dashboard;