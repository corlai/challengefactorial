import * as Sentry from '@sentry/node';

const sentry = Sentry.init({
    dsn: process.env.SENTRY_DSN,
    tracesSampleRate: 1.0
});

export default sentry;
