import mongoose from "mongoose";
import uniqueValidator from "mongoose-unique-validator";

const metricSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    value: {
        type: Number,
        required: true
    },
    timestamp: {
        type: Date,
        required: true
    }
});

metricSchema.plugin(uniqueValidator);
const Metric = mongoose.model("Metric", metricSchema);

export{ Metric };