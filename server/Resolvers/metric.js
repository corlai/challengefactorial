import { UserInputError,ApolloError,ValidationError } from 'apollo-server';
import { Metric } from '../Models/metric.js';
import { GraphQLScalarType, Kind } from 'graphql';

export const metricResolver = {
    DateTime: new GraphQLScalarType({
        name: 'DateTime',
        description: 'Represents a date time object',
        serialize(value) {
            return value.toISOString() // Date to ISOString for JSON
        },
        parseValue(value) {
            return new Date(value) // Integer to Date
        },
        parseLiteral(ast) {
            if (ast.kind === Kind.INT) {
                return new Date(parseInt(ast.value, 10)) // Convert hard-coded AST string to integer and then to Date
            }
            return null // Invalid hard-coded value (not an integer)
        },
    }),
    Query: {
        getMetricsFilter: async (root, args) => {
            let filter = args.filter;

            let hourFilter = filter === 'HR' ? { $hour: "$timestamp" } : { $hour: null };
            let minuteFilter = filter === 'MIN' ? { $minute: "$timestamp" } : { $minute: null };

            try {
                let metrics = await Metric.aggregate([
                    {
                        $group: {
                            _id: {
                                month: { $month: "$timestamp" },
                                day: { $dayOfMonth: "$timestamp" },
                                year: { $year: "$timestamp" },
                                hour: hourFilter,
                                minute: minuteFilter
                            },
                            avgValue: { $avg: "$value" },
                            timestamp: { "$first": { $dateToString: { format: "%Y-%m-%d %H:%M", date: "$timestamp" } } },
                            count: { $sum: 1 }
                        }
                    },
                    {
                        $project: { 
                            timestamp : 1,
                            avgValue : { $round: ['$avgValue', 2] },
                            count: 1
                        },
                    },
                    { $sort: { timestamp: 1 } },
                ]);
    
                return metrics
            } catch (error) {
                throw new UserInputError(error.messages, {
                    invalidArgs: args,
                });
            }

        },
    },
    Mutation: {
        addMetric: async (root, args) => {
            const newMetric = new Metric({ ...args.data });
            try {
                await newMetric.save()
                if(!newMetric) {
                    throw new ApolloError('Metric not saved');
                }
            } catch (error) {
                throw new UserInputError(error.messages, {
                    invalidArgs: args,
                });
            }
            return newMetric;
        },
        generateMetrics: async (root, args) => {
            const { initTime, endTime, quantity } = { ...args.data };
            let arrayMetrics = []
            let resultAddMetrics = []

            while (arrayMetrics.length < quantity) {
                let timestamp = Math.floor(Math.random() * (endTime.getTime() - initTime.getTime()) + initTime.getTime())

                let objMetric = {
                    name: 'met' + timestamp,
                    value: Math.floor(Math.random() * 100),
                    timestamp: new Date(timestamp),
                }

                arrayMetrics.push(objMetric)
            }

            try {
                resultAddMetrics = await Metric.insertMany(arrayMetrics, function (err, res) {
                    if (err) throw err
                    if (!resultAddMetrics) {
                        throw new ApolloError(
                          "Database Error: Could not add metrics",
                        );
                      }
                });
            } catch (error) {
                throw new UserInputError(error.messages,{
                    invalidArgs: args,
                });
            }
            return true
        }

    }
}