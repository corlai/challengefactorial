export const typeDef = `
    scalar DateTime

    enum TypeFilter {
        MIN
        HR
        DAY
    }
    type Metric {
        id: ID!
        timestamp: DateTime
        name: String!
        value: Int!
    }
    type MetricGroupOutput {
        timestamp: String
        avgValue: Float
        count: Int
    }
    input MetricCreateInput {
        name: String
        timestamp: DateTime
        value: Float
    }
    input GenerateMetricsInput {
        initTime: DateTime!
        endTime: DateTime!
        quantity: Int!
    }
    type Query {
        allMetrics: [Metric]
        getMetricsByDay: [MetricGroupOutput]
        getMetricsByHour: [MetricGroupOutput]
        getMetricsByMinute: [MetricGroupOutput]
        getMetricsFilter(filter:TypeFilter): [MetricGroupOutput]
    }
    type Mutation {
        addMetric(data:MetricCreateInput): Metric
        generateMetrics(data:GenerateMetricsInput): Boolean
    }
`;