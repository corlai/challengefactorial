import { ApolloServer, gql } from 'apollo-server';
import { typeDef as Metric } from './Schemas/metric.js';
import { metricResolver } from './Resolvers/metric.js';

import * as Sentry from "@sentry/node";

//initialize libraries
import './Config/dotenv.js';
import './Config/db.js';
import './Config/sentry.js';

const server = new ApolloServer({
    typeDefs: [Metric],
    resolvers: metricResolver,
});

server.listen().then(({ url }) => {
    console.log(`Server ready at ${url}`);
});    
